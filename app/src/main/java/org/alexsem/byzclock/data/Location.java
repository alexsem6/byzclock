package org.alexsem.byzclock.data;

import java.util.Locale;

public class Location {
	private int id;
	private float latitude;
	private float longitude;
	private String name;
	private boolean current;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isCurrent() {
		return current;
	}

	public void setCurrent(boolean current) {
		this.current = current;
	}

	@Override
	public String toString() {
		return (name != null && name.length() > 0) ? name : String.format(Locale.US, "[%.4f, %.4f]", latitude, longitude);
	}

}
