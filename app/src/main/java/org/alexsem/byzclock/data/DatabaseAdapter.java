package org.alexsem.byzclock.data;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseAdapter {

	//Singleton object
	private static DatabaseAdapter instance;
	//Variable to hold the database instance
	private SQLiteDatabase db;
	// Database open/upgrade helper
	private SQLiteOpenHelper dbHelper;

	private static int connectionCount = 0;
	private Object lock = new Object();

	/**
	 * Instantiates singleton object
	 * @param context Context used by the database
	 */
	public static DatabaseAdapter getInstance(Context context) {
		if (instance == null) {
			instance = new DatabaseAdapter(context);
		}
		return instance;
	}

	/**
	 * Constructor
	 * @param context Context used by the database
	 */
	private DatabaseAdapter(Context context) {
		this.dbHelper = new DatabaseHelper(context);
	}

	//--------------------------------------------------------------------------------------

	/**
	 * Opens connection to the database for reading
	 * @throws SQLException if connection was not established
	 */
	public void openRead() throws SQLException {
		synchronized (lock) {
			connectionCount++;
			//System.out.println(String.format("%s: Read %d", Thread.currentThread().getName(), connectionCount));
			db = dbHelper.getReadableDatabase();
		}
	}

	/**
	 * Opens connection to the database for writing
	 * @throws SQLException if connection was not established
	 */
	public void openWrite() throws SQLException {
		synchronized (lock) {
			connectionCount++;
			//System.out.println(String.format("%s: Write %d", Thread.currentThread().getName(), connectionCount));
			db = dbHelper.getWritableDatabase();
		}
	}

	/**
	 * Close connection to the database
	 */
	public void close() {
		synchronized (lock) {
			connectionCount--;
			//System.out.println(String.format("%s: Close %d", Thread.currentThread().getName(), connectionCount));
			if (connectionCount <= 0) {
				connectionCount = 0;
				db.close();
			}
		}
	}

	//--------------------------------------------------------------------------------------

	/**
	 * Insert new location to the database
	 * @param location Location to insert
	 * @return id of inserted location
	 */
	public long insertLocation(Location location) {
		synchronized (lock) {
			ContentValues values = new ContentValues();
			values.put("latitude", location.getLatitude());
			values.put("longitude", location.getLongitude());
			values.put("name", location.getName());
			values.put("current", location.isCurrent() ? 1 : 0);
			return db.insert("Location", null, values);
		}
	}

	/**
	 * Returns specified location from the database
	 * @param id Location identifier
	 * @return Location object
	 */
	public Location selectLocation(int id) {
		synchronized (lock) {
			Location result = null;
			String[] columns = { "id", "latitude", "longitude", "name", "current" };
			String filter = String.format("id = %d", id);
			Cursor cursor = db.query("Location", columns, filter, null, null, null, "id");
			if (cursor.moveToFirst()) { //At least one row present		
				do {
					result = new Location();
					result.setId(cursor.getInt(0));
					result.setLatitude(cursor.getFloat(1));
					result.setLongitude(cursor.getFloat(2));
					result.setName(cursor.getString(3));
					result.setCurrent(cursor.getInt(4) == 1);
				} while (cursor.moveToNext());
			}
			cursor.close();
			return result;
		}
	}

	/**
	 * Returns first available location from the database
	 * @return Location object or null if not found
	 */
	public Location selectCurrentLocation() {
		synchronized (lock) {
			Location result = null;
			String[] columns = { "id", "latitude", "longitude", "name", "current" };
			String filter = "current = 1";
			Cursor cursor = db.query("Location", columns, filter, null, null, null, "id", "1");
			if (cursor.moveToFirst()) { //At least one row present		
				do {
					result = new Location();
					result.setId(cursor.getInt(0));
					result.setLatitude(cursor.getFloat(1));
					result.setLongitude(cursor.getFloat(2));
					result.setName(cursor.getString(3));
					result.setCurrent(true);
				} while (cursor.moveToNext());
			}
			cursor.close();
			return result;
		}
	}

	/**
	 * Returns list of locations
	 * @return list of available locations
	 */
	public List<Location> selectLocationList() {
		synchronized (lock) {
			List<Location> result = new ArrayList<Location>();
			String[] columns = { "id" };
			Cursor cursor = db.query("Location", columns, null, null, null, null, "current desc, id");
			if (cursor.moveToFirst()) { //At least one row present		
				do {
					result.add(selectLocation(cursor.getInt(0)));
				} while (cursor.moveToNext());
			}
			cursor.close();
			return result;
		}
	}

	/**
	 * Returns list of locations which does not have a name yet
	 * @return list of nameless locations
	 */
	public List<Location> selectUnnamedLocationList() {
		synchronized (lock) {
			List<Location> result = new ArrayList<Location>();
			String[] columns = { "id" };
			String filter = "current = 0 and (name is null or name = '')";
			Cursor cursor = db.query("Location", columns, filter, null, null, null, null);
			if (cursor.moveToFirst()) { //At least one row present		
				do {
					result.add(selectLocation(cursor.getInt(0)));
				} while (cursor.moveToNext());
			}
			cursor.close();
			return result;
		}
	}

	/**
	 * Updates specified location to be the new current
	 * @param id Location identifier
	 */
	public void updateLocationSelected(int id) {
		synchronized (lock) {
			ContentValues values = new ContentValues();
			values.put("current", 0);
			db.update("Location", values, null, null);
			String filter = String.format("id = %d", id);
			values.put("current", 1);
			db.update("Location", values, filter, null);
		}
	}

	/**
	 * Updates name of specified location
	 * @param id Location identifier
	 * @param name New location name
	 */
	public void updateLocationName(int id, String name) {
		synchronized (lock) {
			ContentValues values = new ContentValues();
			values.put("name", name);
			String filter = String.format("id = %d", id);
			db.update("Location", values, filter, null);
		}
	}

	/**
	 * Deletes specified location
	 * @param id Location identifier
	 */
	public void deleteLocation(int id) {
		synchronized (lock) {
			String filter = String.format("id = %d", id);
			db.delete("Location", filter, null);
		}
	}

}
