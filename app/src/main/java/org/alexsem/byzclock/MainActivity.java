package org.alexsem.byzclock;

import java.util.Calendar;
import java.util.Locale;

import org.alexsem.byzclock.data.DatabaseAdapter;
import org.alexsem.byzclock.data.Location;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;
import android.widget.TextView;

import com.luckycatlabs.sunrisesunset.SunriseSunsetCalculator;

public class MainActivity extends Activity {

	private final int REQUEST_LOCATION = 123;

	private SlidingDrawer sldData;

	private DatabaseAdapter database;
	private Location current = null;

	private ImageView imgClock;
	private TextView txtByz;
	private TextView txtError;

	private TextView txtDate;
	private TextView txtSunrise;
	private TextView txtSunset;
	private TextView txtDuration;
	private Button btnLocation;

	private boolean running = true;
	private boolean refreshNow = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		//--- Data panel views ---
		sldData = (SlidingDrawer) findViewById(R.id.data);
		txtDate = (TextView) findViewById(R.id.data_date);
		txtSunrise = (TextView) findViewById(R.id.data_sunrise);
		txtSunset = (TextView) findViewById(R.id.data_sunset);
		txtDuration = (TextView) findViewById(R.id.data_duration);
		btnLocation = (Button) findViewById(R.id.data_location);
		btnLocation.setOnClickListener(locationOnClickListener);

		//--- Main panel views ---
		imgClock = (ImageView) findViewById(R.id.main_clock);
		imgClock.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (sldData.isOpened()) {
					sldData.animateClose();
					return true;
				}
				return false;
			}
		});
		txtByz = (TextView) findViewById(R.id.main_byz);
		txtByz.setTypeface(Typeface.createFromAsset(getAssets(), "ds-digital.ttf"));
		txtError = (TextView) findViewById(R.id.main_error);

		//--- Load current location data from database ---
		database = DatabaseAdapter.getInstance(this);
		database.openWrite();
		current = database.selectCurrentLocation();
		for (Location location : database.selectUnnamedLocationList()) {
			new AddressFetchTask(null).execute(location);
		}
		database.close();
		if (current == null) { //Location not defined
			btnLocation.performClick();
		} else if (current.getName() == null || current.getName().length() == 0) {
			new AddressFetchTask(btnLocation).execute(current);
		}

		//--- Start updating fields ---
		new CalculatorTask().execute();
	}

	/**
	 * Listener for location button clicks
	 */
	private OnClickListener locationOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			startActivityForResult(new Intent(MainActivity.this, LocationActivity.class), REQUEST_LOCATION);
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_LOCATION /*&& resultCode == LocationActivity.RES*/) { //Need to update location
			database.openRead();
			current = database.selectCurrentLocation();
			database.close();
			refreshNow = true;
			if (current != null && (current.getName() == null || current.getName().length() == 0)) {
				new AddressFetchTask(btnLocation).execute(current);
			}
		}

	}

	@Override
	public void onBackPressed() {
		if (sldData.isOpened()) {
			sldData.animateClose();
			return;
		}
		running = false;
		super.onBackPressed();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		running = false;
	}

	//-----------------------------------------------------------

	/**
	 * Task that is used for Byzantine time calculations
	 * @author Semeniuk A.D.
	 */
	private class CalculatorTask extends AsyncTask<Void, Integer, Void> {

		private final int RATE = 5;

		@Override
		protected void onProgressUpdate(Integer... values) {
			txtDate.setText(Calculator.calendarToString(Calendar.getInstance(Locale.getDefault())));

			if (current != null && values[0] > -1) { //Results present

				//--- Define visibility for fields ---
				txtByz.setVisibility(View.VISIBLE);
				txtError.setVisibility(View.GONE);
				txtSunrise.setVisibility(View.VISIBLE);
				txtSunset.setVisibility(View.VISIBLE);
				txtDuration.setVisibility(View.VISIBLE);

				//--- Populate data to the fields ---
				txtSunrise.setText(Calculator.seconds2String(values[1]));
				txtSunset.setText(Calculator.seconds2String(values[2]));
				txtDuration.setText(Calculator.seconds2String(values[3]));
				btnLocation.setText(current.toString());
				txtByz.setText(Calculator.seconds2String(values[4]));
				imgClock.setTag(Calculator.getAngle(values[4]));

			} else { //Nothing calculated

				//--- Define visibility for fields ---
				txtByz.setVisibility(View.GONE);
				txtError.setVisibility(View.VISIBLE);
				txtSunrise.setVisibility(View.GONE);
				txtSunset.setVisibility(View.GONE);
				txtDuration.setVisibility(View.GONE);

				//--- Populate data to the fields ---
				imgClock.setTag(null);
				btnLocation.setText(R.string.data_location_none);
			}

			//--- Request changing margins of Sliding Drawer ---
			sldData.postDelayed(new Runnable() {
				@Override
				public void run() {
					((RelativeLayout.LayoutParams) sldData.getLayoutParams()).topMargin = ((View) sldData.getParent()).getHeight()
							- ((ViewGroup) sldData.getContent()).getChildAt(0).getHeight() - sldData.getHandle().getHeight();
					sldData.requestLayout();
				}
			}, 500);
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			while (MainActivity.this.running) {

				if (current != null) { //Location defined
					//--- Define current date ---
					Calendar now = Calendar.getInstance(Locale.getDefault());
					//--- Calculate sun rise and sun set times ---
					com.luckycatlabs.sunrisesunset.dto.Location location = new com.luckycatlabs.sunrisesunset.dto.Location(current.getLatitude(),
							current.getLongitude());
					SunriseSunsetCalculator calculator = new SunriseSunsetCalculator(location, now.getTimeZone());
					Calendar rise = calculator.getOfficialSunriseCalendarForDate(now);
					Calendar set = calculator.getOfficialSunsetCalendarForDate(now);
                    if (rise != null && set != null){
                        //--- Calculate all necessary data ---
                        int currentS = Calculator.timeAsSeconds(now);
                        int riseS = Calculator.timeAsSeconds(rise);
                        int setS = Calculator.timeAsSeconds(set);
                        int durationS = Calculator.calculateDurationAsSeconds(riseS, setS);
                        int byzS = Calculator.calculateByzTimeAsSeconds(currentS, riseS, setS);
                        //--- Publish progress---
                        publishProgress(1, riseS, setS, durationS, byzS);
                    }
				} else { //Location is not yet defined
					//--- Publish progress---
					publishProgress(0);
				}

				refreshNow = false;
				try {
					for (int i = 0; i < RATE && !refreshNow; i++)
						Thread.sleep(1000);
				} catch (Exception ex) {
				}

			}
			return null;
		}

	}

	//-----------------------------------------------------------

	/**
	 * Task that is used for current address fetching
	 * @author Semeniuk A.D.
	 */
	private class AddressFetchTask extends AsyncTask<Location, Void, Location> {

		private Button button;

		AddressFetchTask(Button button) {
			this.button = button;
		}

		@Override
		protected void onPostExecute(Location result) {
			if (result != null) { //Success
				database.openWrite();
				database.updateLocationName(result.getId(), result.getName());
				database.close();
				if (button != null) {
					button.setText(result.getName());
				}
			}
		}

		@Override
		protected Location doInBackground(Location... params) {
			Location location = params[0];
			location.setName(Calculator.defineAddress(MainActivity.this, location.getLatitude(), location.getLongitude()));
			return location;
		}

	}

}