package org.alexsem.byzclock.widget;

import java.util.Calendar;
import java.util.TimeZone;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;

public abstract class ClockWidget extends AppWidgetProvider {
	private PendingIntent service = null;
	private final int RATE = 10000;

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		final AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		final Calendar now = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		final Intent intent = new Intent(context, WidgetService.class);
		intent.putExtra("class", this.getClass().getName());
		if (service == null) {
			service = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
		}
		manager.setRepeating(AlarmManager.RTC, now.getTimeInMillis(), RATE, service);
	}

	@Override
	public void onDisabled(Context context) {
		final AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		manager.cancel(service);
	}

}