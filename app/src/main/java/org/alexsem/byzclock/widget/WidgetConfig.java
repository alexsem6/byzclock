package org.alexsem.byzclock.widget;

import org.alexsem.byzclock.LocationActivity;
import org.alexsem.byzclock.data.DatabaseAdapter;
import org.alexsem.byzclock.data.Location;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.Bundle;

public class WidgetConfig extends Activity {
	private DatabaseAdapter database;
	private final int REQUEST_LOCATION = 123;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		database = DatabaseAdapter.getInstance(this);
		database.openRead();
		Location current = database.selectCurrentLocation();
		database.close();
		if (current == null) { //Need to set up location
			startActivityForResult(new Intent(this, LocationActivity.class), REQUEST_LOCATION);
		} else { //Location set
			endConfig();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_LOCATION) {
			endConfig();
		}
	}

	private void endConfig() {
		int id = getIntent().getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
		Intent resultValue = new Intent();
		resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, id);
		setResult(RESULT_OK, resultValue);
		finish();
	}

}
