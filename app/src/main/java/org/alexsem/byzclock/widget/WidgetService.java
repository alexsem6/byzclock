package org.alexsem.byzclock.widget;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.os.AsyncTask;
import android.os.IBinder;
import android.widget.RemoteViews;

import com.luckycatlabs.sunrisesunset.SunriseSunsetCalculator;

import org.alexsem.byzclock.Calculator;
import org.alexsem.byzclock.MainActivity;
import org.alexsem.byzclock.R;
import org.alexsem.byzclock.data.DatabaseAdapter;
import org.alexsem.byzclock.data.Location;

import java.util.Calendar;
import java.util.Locale;

public class WidgetService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            Class<?> clazz = Class.forName(intent.getStringExtra("class"));
            new BitmapCreatorTask(clazz).execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Task that is used for widget update
     * @author Semeniuk A.D.
     */
    private class BitmapCreatorTask extends AsyncTask<Void, Void, Bitmap> {

        private Class<?> clazz;

        public BitmapCreatorTask(Class<?> clazz) {
            this.clazz = clazz;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            //--- Generate bitmap from a resource ---
            Bitmap bitmap = BitmapFactory.decodeResource(WidgetService.this.getResources(), R.drawable.clock250);
            if (bitmap != null) {
                bitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
            }
            if (bitmap == null) {
                return null;
            }
            try {
                //--- Read current location from the database ---
                DatabaseAdapter database = DatabaseAdapter.getInstance(WidgetService.this);
                database.openRead();
                Location current = database.selectCurrentLocation();
                database.close();

                if (current != null) { //Location defined
                    //--- Define current date ---
                    Calendar now = Calendar.getInstance(Locale.getDefault());
                    //--- Calculate sun rise and sun set times ---
                    com.luckycatlabs.sunrisesunset.dto.Location location = new com.luckycatlabs.sunrisesunset.dto.Location(current.getLatitude(),
                            current.getLongitude());
                    SunriseSunsetCalculator calculator = new SunriseSunsetCalculator(location, now.getTimeZone());
                    Calendar rise = calculator.getOfficialSunriseCalendarForDate(now);
                    Calendar set = calculator.getOfficialSunsetCalendarForDate(now);
                    //--- Calculate all necessary data ---
                    int currentS = Calculator.timeAsSeconds(now);
                    int riseS = Calculator.timeAsSeconds(rise);
                    int setS = Calculator.timeAsSeconds(set);
                    int byzS = Calculator.calculateByzTimeAsSeconds(currentS, riseS, setS);
                    //--- Define final angle ---
                    Float angle = Calculator.getAngle(byzS);

                    //--- Creating Paint and canvas ---
                    Paint paint = new Paint();
                    paint.setAntiAlias(true);
                    paint.setColor(Color.RED);
                    Canvas canvas = new Canvas(bitmap);
                    //--- Pre-calculating values
                    int w = bitmap.getWidth();
                    float cX = w / 2f;
                    float cY = w / 2f * (125f / 126f);
                    float r = 5f * w / 250f;
                    float s = 3f * w / 250f;
                    float r2 = 84f * w / 250f;
                    float sX = cX + r2 * (float) Math.cos(Math.toRadians(angle));
                    float sY = cY + r2 * (float) Math.sin(Math.toRadians(angle));
                    //--- Painting central ring ---
                    paint.setStyle(Style.FILL);
                    canvas.drawCircle(cX, cY, r, paint);
                    //--- Painting clock hand ---
                    paint.setStrokeWidth(s);
                    paint.setStyle(Style.STROKE);
                    canvas.drawLine(cX, cY, sX, sY, paint);
                }
            } catch (Exception ex) {
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (bitmap != null) {
                //--- Get remote views binding information ---
                RemoteViews remoteViews = new RemoteViews(WidgetService.this.getPackageName(), R.layout.widget);
                ComponentName watchWidget = new ComponentName(WidgetService.this, clazz);
                //--- Create an Intent to launch MainActivity ---
                Intent intent = new Intent(WidgetService.this, MainActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(WidgetService.this, 0, intent, 0);
                //--- Set data and listeners for desired view ---
                remoteViews.setOnClickPendingIntent(R.id.widget_clock, pendingIntent);
                remoteViews.setImageViewBitmap(R.id.widget_clock, bitmap);
                //--- Update widgets ---
                AppWidgetManager manager = AppWidgetManager.getInstance(WidgetService.this);
                manager.updateAppWidget(watchWidget, remoteViews);
            }
        }

    }
}
