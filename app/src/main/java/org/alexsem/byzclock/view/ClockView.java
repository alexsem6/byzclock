package org.alexsem.byzclock.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ClockView extends ImageView {

	public ClockView(Context context) {
		super(context);
		init(context);
	}

	public ClockView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public ClockView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}
	
	@Override
	public void setTag(Object tag) {
		super.setTag(tag);
		this.invalidate();
	}

	private Paint paint;

	private void init(Context context) {
		paint = new Paint();
		paint.setAntiAlias(true);
		paint.setColor(0xffe34234);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		int w = this.getWidth();

		float cX = w / 2f;
		float cY = w / 2f * (375f / 368f);
		float r = 15f * w / 737f;
		float s = 9f * w / 737f;
		float r2 = 250f * w / 737f;

		if (this.getTag() != null) { //Need to draw a line
			paint.setStyle(Style.FILL);
			canvas.drawCircle(cX, cY, r, paint);
			float a = ((Float) this.getTag()).floatValue();
			float sX = cX + r2 * (float) Math.cos(Math.toRadians(a));
			float sY = cY + r2 * (float) Math.sin(Math.toRadians(a));
			paint.setStrokeWidth(s);
			paint.setStyle(Style.STROKE);
			canvas.drawLine(cX, cY, sX, sY, paint);
		}
	}

}
