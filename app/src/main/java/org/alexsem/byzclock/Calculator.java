package org.alexsem.byzclock;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

public class Calculator {

	private static final long S_HALF_DAY = 12 * 60 * 60;
	private static final int S_DAY = 24 * 60 * 60;
	private static final int S_MORNING = 6 * 60 * 60;
	private static final int S_EVENING = 18 * 60 * 60;

	/**
	 * Converts time to number of seconds passed since midnight
	 * @param time Time to convert
	 * @return Number of seconds
	 */
	public static int timeAsSeconds(Calendar time) {
		return time.get(Calendar.HOUR_OF_DAY) * 3600 + time.get(Calendar.MINUTE) * 60 + time.get(Calendar.SECOND);
	}

	/**
	 * Converts date and time to string
	 * @param datetime Date and time to convert
	 * @return String representation
	 */
	public static String calendarToString(Calendar datetime) {
		return String.format("%1$te %1$tB %1$tY %1$tH:%1$tM", datetime);
	}

	/**
	 * Converts time defined as seconds to HH:mm string
	 * @param timeS Seconds to convert
	 * @return String representation (HH:mm format)
	 */
	public static String seconds2String(int timeS) {
		return String.format("%02d:%02d", timeS / 3600, (timeS / 60) % 60);
	}

	/**
	 * Calculate duration of the day time in seconds
	 * @param riseS Sunrise time in seconds
	 * @param setS Sunset time in seconds
	 * @return Duration in seconds
	 */
	public static int calculateDurationAsSeconds(int riseS, int setS) {
		return (setS > riseS ? setS : setS + S_DAY) - riseS;
	}

	/**
	 * Calculates current Byzantine time
	 * @param currentS Current time as seconds
	 * @param riseS Sunrise time as seconds
	 * @param setS Sunset time as seconds
	 * @return Current Byzantine time as seconds
	 */
	public static int calculateByzTimeAsSeconds(int currentS, int riseS, int setS) {
		long byzS;
		int durationDayS = setS - riseS;
		int durationNightS = S_DAY - durationDayS;
		if (currentS < riseS) { //Night (1st part)
			byzS = (currentS + S_DAY - setS) * S_HALF_DAY / durationNightS + S_EVENING;
		} else if (currentS < setS) { //Day time
			byzS = (currentS - riseS) * S_HALF_DAY / durationDayS + S_MORNING;
		} else { //Night time (2nd part)
			byzS = (currentS - setS) * S_HALF_DAY / durationNightS + S_EVENING;
		}
		byzS %= S_DAY;
		return (int)byzS;
	}

	/**
	 * Returns angle of clock hand for current time
	 * @param byzS Current Byzantine time
	 * @return Hand angle
	 */
	public static float getAngle(int byzS) {
		return byzS * 360f / S_DAY;
	}

	
	/**
	 * Defines address string which corresponds to specified location
	 * @param context Context
	 * @param latitude Location latitude
	 * @param longitude Location longitude
	 * @return Address string (if any) 
	 */
	public static String defineAddress(Context context, double latitude, double longitude) {
		try {
			Geocoder geocoder = new Geocoder(context, Locale.getDefault());
			List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
			if (addresses != null && addresses.size() > 0) {
				Address address = addresses.get(0);
				String cc = address.getCountryName();
				String ll = address.getLocality();
				if (ll == null) {
					ll = address.getAddressLine(0);
				}
				if (cc != null && ll != null) {
					return String.format("%s, %s", ll, cc);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

}
