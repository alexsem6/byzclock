package org.alexsem.byzclock.adapter;

import java.util.List;

import org.alexsem.byzclock.R;
import org.alexsem.byzclock.data.Location;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Implementation of own adapter for list of locations
 * @author Semeniuk A.D.
 */
public class LocationAdapter extends ArrayAdapter<Location> {
	private LayoutInflater inflater = null;
	private OnClickListener rowClickListener;
	private OnClickListener deleteClickListener;

	/**
	 * Constructor
	 * @param list Elements of list
	 */
	public LocationAdapter(Context context, List<Location> list) {
		super(context, R.layout.location_item, list);
		this.inflater = LayoutInflater.from(context);
	}

	public void setRowClickListener(OnClickListener rowClickListener) {
		this.rowClickListener = rowClickListener;
	}

	public void setDeleteClickListener(OnClickListener deleteClickListener) {
		this.deleteClickListener = deleteClickListener;
	}

	/**
	 * Performs updates to make new item selected
	 * @param item New selected item
	 */
	public void updateSelected(Location item) {
		for (int i = 0; i < getCount(); i++) {
			getItem(i).setCurrent(false);
		}
		item.setCurrent(true);
		remove(item);
		insert(item, 0);
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder wrapper = null;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.location_item, parent, false);
			wrapper = new ViewHolder(convertView);
			convertView.setTag(R.id.location_item_name, wrapper);
		} else {
			wrapper = (ViewHolder) convertView.getTag(R.id.location_item_name);
		}

		Location item = this.getItem(position);
		wrapper.getDelete().setTag(position);
		wrapper.getDelete().setOnClickListener(deleteClickListener);
		wrapper.getName().setTypeface(null, item.isCurrent() ? Typeface.BOLD : Typeface.NORMAL);
		wrapper.getName().setTextColor(item.isCurrent() ? 0xff5c3921 : 0xff806348);
		wrapper.getName().setText(item.toString());
		convertView.setOnClickListener(rowClickListener);
		convertView.setTag(position);
		convertView.setFocusable(false);
		return convertView;
	}

	//------------------------------------------------------------

	/**
	 * Helper class for holding view links
	 * @author Semeniuk A.D.
	 */
	private class ViewHolder {
		private View base;
		private TextView name = null;
		private ImageView delete = null;

		public ViewHolder(View base) {
			this.base = base;
		}

		public TextView getName() {
			if (name == null) {
				name = (TextView) base.findViewById(R.id.location_item_name);
			}
			return (name);
		}

		public ImageView getDelete() {
			if (delete == null) {
				delete = (ImageView) base.findViewById(R.id.location_item_delete);
			}
			return (delete);
		}

	}

}