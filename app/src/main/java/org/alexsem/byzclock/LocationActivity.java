package org.alexsem.byzclock;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.luckycatlabs.sunrisesunset.SunriseSunsetCalculator;

import org.alexsem.byzclock.adapter.LocationAdapter;
import org.alexsem.byzclock.data.DatabaseAdapter;
import org.alexsem.byzclock.data.Location;

import java.util.Calendar;
import java.util.Locale;

public class LocationActivity extends Activity {

    private DatabaseAdapter database;

    private ListView lstLocation;
    private Button btnChoose;
    private Button btnAdd;

    private boolean changesMade = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location);

        //--- Location list and button ---
        lstLocation = (ListView) findViewById(R.id.location_list);
        btnAdd = (Button) findViewById(R.id.location_add);
        btnAdd.setOnClickListener(addClickListener);
        btnChoose = (Button) findViewById(R.id.location_choose);
        btnChoose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(changesMade ? Activity.RESULT_OK : Activity.RESULT_CANCELED);
                LocationActivity.this.finish();
            }
        });

        //--- Load location data from the database ---
        database = DatabaseAdapter.getInstance(this);
        database.openWrite();
        LocationAdapter adapter = new LocationAdapter(LocationActivity.this, database.selectLocationList());
        database.close();
        adapter.setRowClickListener(itemNameOnClickListener);
        adapter.setDeleteClickListener(itemDeleteOnClickListener);
        lstLocation.setAdapter(adapter);

    }

    /**
     * Listener for location button clicks
     */
    private OnClickListener addClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog d = new AlertDialog.Builder(LocationActivity.this).setItems(new String[]{getString(R.string.location_add_auto), getString(R.string.location_add_manual)},
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
                            switch (which) {
                                case 0: //Auto
                                    if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(LocationActivity.this) == ConnectionResult.SUCCESS) {
                                        String providers = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
                                        if (providers.contains("gps") || providers.contains("network")) { //Provider enabled
                                            startDefiningLocation();
                                        } else { //Provider disabled
                                            Toast.makeText(LocationActivity.this, R.string.location_add_enable, Toast.LENGTH_SHORT).show();
                                            startActivity(new Intent(Settings.ACTION_SECURITY_SETTINGS));
                                        }
                                    } else {
                                        Toast.makeText(LocationActivity.this, R.string.location_add_playservices, Toast.LENGTH_SHORT).show();
                                    }
                                    break;
                                case 1: //Manual
                                    View view = getLayoutInflater().inflate(R.layout.location_add, null);
                                    final EditText latitude = (EditText) view.findViewById(R.id.location_add_manual_lat);
                                    final EditText longitude = (EditText) view.findViewById(R.id.location_add_manual_long);
                                    final AlertDialog dialog = new AlertDialog.Builder(LocationActivity.this).setTitle(R.string.location_enter_coords).setView(view)
                                            .setPositiveButton(R.string.location_search, null).setNegativeButton(R.string.location_cancel, null).create();
                                    dialog.show();
                                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                                        @SuppressLint("NewApi")
                                        @Override
                                        public void onClick(View v) {
                                            float lat;
                                            float lon;

                                            if (latitude.getText().length() == 0) {
                                                latitude.setText("0");
                                            }
                                            if (longitude.getText().length() == 0) {
                                                longitude.setText("0");
                                            }
                                            try {
                                                lat = Float.valueOf(latitude.getText().toString());
                                                if (lat < -90f || lat > 90f) {
                                                    throw new Exception();
                                                }
                                            } catch (Exception ex) {
                                                Toast.makeText(LocationActivity.this, R.string.location_add_failedlat, Toast.LENGTH_SHORT).show();
                                                return;
                                            }
                                            try {
                                                lon = Float.valueOf(longitude.getText().toString());
                                                if (lon < -180f || lon > 180f) {
                                                    throw new Exception();
                                                }
                                            } catch (Exception ex) {
                                                Toast.makeText(LocationActivity.this, R.string.location_add_failedlon, Toast.LENGTH_SHORT).show();
                                                return;
                                            }
                                            Location location = new Location();
                                            location.setLatitude(lat);
                                            location.setLongitude(lon);
                                            if (!checkLocation(location)) {
                                                Toast.makeText(LocationActivity.this, R.string.location_add_norise, Toast.LENGTH_LONG).show();
                                                return;
                                            }
                                            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
                                                new FindNameTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, location);
                                            } else {
                                                new FindNameTask().execute(location);
                                            }
                                            dialog.dismiss();
                                        }
                                    });
                                    break;
                            }

                        }
                    }).create();
            d.setCanceledOnTouchOutside(true);
            d.show();
        }
    };

    private LocationClient client;
    private ProgressDialog progress;

    @SuppressLint("NewApi")
    private void startDefiningLocation() {

        progress = new ProgressDialog(LocationActivity.this);
        progress.setMessage(getString(R.string.location_add_defining));
        progress.setCancelable(true);
        progress.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                client.disconnect();
            }
        });
        progress.show();


        client = new LocationClient(LocationActivity.this, new GooglePlayServicesClient.ConnectionCallbacks() {
            @Override
            public void onConnected(Bundle bundle) {
                if (progress != null) {
                    progress.dismiss();
                }
                android.location.Location location = client.getLastLocation();
                client.disconnect();
                if (location == null) {
                    Toast.makeText(LocationActivity.this, R.string.location_add_define_fail, Toast.LENGTH_LONG).show();
                    return;
                }
                Location loc = new Location();
                loc.setLatitude((float) location.getLatitude());
                loc.setLongitude((float) location.getLongitude());
                if (!checkLocation(loc)) {
                    Toast.makeText(LocationActivity.this, R.string.location_add_norise, Toast.LENGTH_LONG).show();
                    return;
                }
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
                    new FindNameTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, loc);
                } else {
                    new FindNameTask().execute(loc);
                }
            }

            @Override
            public void onDisconnected() {

            }
        }, new GooglePlayServicesClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(ConnectionResult connectionResult) {
                if (progress != null) {
                    progress.dismiss();
                }
                Toast.makeText(LocationActivity.this, R.string.location_add_define_fail, Toast.LENGTH_SHORT).show();
            }
        });
        client.connect();
    }

    /**
     * Asynchronous task used for location definition
     */
    private class FindNameTask extends AsyncTask<Location, Void, Location> {

        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(LocationActivity.this);
            dialog.setMessage(getString(R.string.location_add_searching));
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Location doInBackground(Location... params) {
            Location loc = params[0];
            loc.setName(Calculator.defineAddress(LocationActivity.this, loc.getLatitude(), loc.getLongitude()));
            return loc;
        }

        @Override
        protected void onPostExecute(Location result) {
            if (dialog != null) {
                dialog.dismiss();
            }
            if (TextUtils.isEmpty(result.getName())) {
                result.setName(getString(R.string.location_unknown));
            }
            showFoundDialog(result);
        }
    }

    /**
     * Checks whether specified location is "valid"
     * (in terms Byzantee time can be calculated in it)
     * @param location Location to check
     * @return true if location is valid, false otherwise
     */

    private boolean checkLocation(Location location) {
        Calendar now = Calendar.getInstance();
        com.luckycatlabs.sunrisesunset.dto.Location l = new com.luckycatlabs.sunrisesunset.dto.Location(location.getLatitude(), location.getLongitude());
        SunriseSunsetCalculator calculator = new SunriseSunsetCalculator(l, now.getTimeZone());
        Calendar rise = calculator.getOfficialSunriseCalendarForDate(now);
        Calendar set = calculator.getOfficialSunsetCalendarForDate(now);
        return rise != null && set != null;
    }

    public void showFoundDialog(final Location location) {
        new AlertDialog.Builder(this).setTitle(R.string.location_found).setIcon(android.R.drawable.ic_dialog_info)
                .setMessage(String.format(Locale.US, "%s\n(%.4f, %.4f)", location.getName(), location.getLatitude(), location.getLongitude()))
                .setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        LocationAdapter adapter = (LocationAdapter) lstLocation.getAdapter();
                        location.setCurrent(true);
                        database.openWrite();
                        location.setId((int) database.insertLocation(location));
                        database.updateLocationSelected(location.getId());
                        database.close();
                        adapter.insert(location, 0);
                        adapter.updateSelected(location);
                        lstLocation.setSelectionFromTop(0, 0);
                        changesMade = true;
                    }
                }).setNegativeButton(R.string.location_cancel, null).show();
    }

    /**
     * Listener for location item clicks
     */
    private OnClickListener itemNameOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            LocationAdapter adapter = (LocationAdapter) lstLocation.getAdapter();
            Location location = adapter.getItem((Integer) v.getTag());
            boolean isCurrent = location.isCurrent();
            database.openWrite();
            database.updateLocationSelected(location.getId());
            database.close();
            adapter.updateSelected(location);
            lstLocation.setSelectionFromTop(0, 0);
            changesMade = true;
            if (isCurrent) { //Chose current location
                btnChoose.performClick();
            }
        }
    };

    /**
     * Listener for location delete clicks
     */
    private OnClickListener itemDeleteOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            final int position = (Integer) v.getTag();
            new AlertDialog.Builder(LocationActivity.this).setTitle(R.string.location_delete_title).setIcon(android.R.drawable.ic_dialog_alert)
                    .setMessage(R.string.location_delete_message).setPositiveButton(R.string.location_yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    LocationAdapter adapter = (LocationAdapter) lstLocation.getAdapter();
                    Location item = adapter.getItem(position);
                    database.openWrite();
                    database.deleteLocation(item.getId());
                    database.close();
                    changesMade = true;
                    adapter.remove(item);
                }
            }).setNegativeButton(R.string.location_no, null).show();
        }
    };

    @Override
    public void onBackPressed() {
        btnChoose.performClick();
    }

}
